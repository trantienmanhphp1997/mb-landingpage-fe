export const state = () => ({
  isShow: 0,
  jobDesModal: null,
  confirm: null,
  confirmModal: null
})

export const mutations = {
  setIsShow (state, e) {
    state.isShow = e
  },
  initIsShow (state) {
    state.isShow = 0
  },

  setJobDesModal (state, data) {
    state.jobDesModal = data
  },
  initJobDesModal (state) {
    state.jobDesModal = null
  },

  setConfirmModal (state, data) {
    state.confirmModal = data
  },
  initConfirmModal (state) {
    state.confirmModal = null
  }
}
