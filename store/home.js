export const state = () => ({
  isShow: 0,
  modalData: null,
  banner_data: null,
  fmous_people_data: null,
  footer_data: null,
  mbit_review_data: null,
  reason_data: null,
  recruitment_data: null,
  scholarship_step_data: null,
  selection_round_data: null,
  scholarship_fund: null,
  top_menu_data: null
})

export const mutations = {
  setIsShow (state, e) {
    state.isShow = e
  },
  initIsShow (state) {
    state.isShow = 0
  },

  setModalData (state, data) {
    state.modalData = data
  },
  initModalData (state) {
    state.modalData = null
  },

  set_footer_data (state, data) {
    state.footer_data = data
  },
  init_footer_data (state) {
    state.footer_data = null
  },

  set_banner_data (state, data) {
    state.banner_data = data
  },
  init_banner_data (state) {
    state.banner_data = null
  },

  set_top_menu_data (state, data) {
    state.top_menu_data = data
  },
  init_top_menu_data (state) {
    state.top_menu_data = null
  }
}
