// JavaScript Document
//----------------nav---------------
$(function(){
  ud = undefined;
  String.prototype.isJSON = function() {
    return iA(this[0] + this[this.length - 1], ["{}", "[]"])
  }

  iA = inArray = function(n, h, e) {
    if (!h)
      return;
    if (!$.isArray(h)) {
      if (typeof h == "object" && h.l) {
        h = h.l.concat(
          $.map(h, function(á, à) {
            if (à >= 0)
              return á
          })
        );
      } else {
        return false
      }
    };
    if ($.isArray(n)) {
        var s = sum(n.map(function(á) {
            return iA(á, h) ? 1 : 0
        }));
        return e ? s == n.length : s > 0
    };
    var r = false;
    $.each(h, function(i, v) {
      if ((!e && v == n) || (e && v === n) || (Number(v) == n)) {
        r = i + 1;
        return false
      }
    });
    return r
  }
  Rd = function(a, b) {
    if ($.isArray(a))
        return a[b > 0 ? b % (a.length + 1) : Rd(0, a.length - 1)];
    a = a || 0;
    b = b || 0;
    return Math.abs((Math.random() * (b + 1)) + a)
  }

  $.n_loop = $.n_duplicate = function() {
    var a = arguments,
      r = [],
      o = {};
    if (a) {
      $.map(a, function(á) {
        if (á) {
          switch (typeof á) {
            case "object":
              if ($.isArray(á)) {
                $.map(á, function(à) {
                  if (!o[à])
                    r.push(à);
                  o[à] = 1;
                });
              }
            break;
          case "number":
          case "string":
            if (!o[á])
              r.push(á);
            o[á] = 1;
            break;
          }
        }
      })
    }
    return r
  }
  tF = function(a, b) {
    return (a * 1).toFixed(b) * 1
  }

  rf = kd("days")
  function sum() {
    var r = 0;
    $.map(arguments, function(á) {
        switch (typeof á) {
        case "number":
        case "string":
            if (á * 1 == á) {
                r += á * 1
            }
            ;break;
        case "object":
            if ($.isArray(á)) {
                r += sum.apply(ud, á)
            }
            ;break
        }
    });
    return tF(r, 10)
  }
  
  function kd(a) {
    return function() {
        return this._data[a] || NaN;
    }
  }

  function classM(w) {
    var e = w < 991;
    $('[classM]').each(function(){
      var x = $(this),
        c = x.attr("classM");
      c.split(",").map(function(v){
        var m = /^\-/,
          r = v.trim().replace(m, '');
        if(v.match(m)) // Có dấu - thì remove
          x[e ? 'removeClass' : 'addClass'](r);
        else
          x[e ? 'addClass' : 'removeClass'](r);
      });
    });
  };

  function isMedia(m) {
    var t = typeof this == "string",
      r = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Silk|PlayBook|Mobile|Tablet/i,
      u = m || navigator.userAgent || navigator.vendor || window.opera;
    return r.test(t ? this : u);
  }

  function is(a) {
    if(!a)
      return false;
    switch(a){
      case "M":
        return isMedia();
      break;
      default:
        return false;
      break;
    }
  }

  Jd = jS = function(x, y) {
    if (empty(x))
      return y ? {} : [];
    if (typeof x == "object")
      return x;
    try {
      x = JSON.parse(typeof x == "string" && x.isJSON() ? x : y ? "{}" : "[]")
    } catch (e) {
      x = $.parseJSON(x) || (y ? {} : [])
    };
    return x;
  }

  function arr(x, y) {
    if ($.isArray(x))
        return x;
    x = (x || "") + "";
    if (!x)
        return [];
    if (typeof x == "string" && x.isJSON())
        return Jd(x);
    return x.split(y || ",")
  }
  
  $.extend($.fn, {
    removeCSS: function(v, f) {
      return this.each(function() {
          var s = this.style, 
            r = {};
          arr(v).map(function(á) {
            r[á] = s.removeProperty(á)
          });
          if(f){
            f.call(this, r)
          }
      })
    },
    self: function(e, f, g) {
      return this.each(function() {
        if (g) {
          $(this).on(e, f, function(a) {
            if (a.target == this)
                g.apply(this, arguments)
          })
        } else {
          $(this).on(e, function(a) {
            if (a.target == this)
              f.apply(this, arguments)
          })
        }
      })
    },
    changeClass: function(s, y) {
      return this.each(function() {
        if (!s)
          return;
        var t = $(this);
        if (!$.isArray(s))
          s = (s + "").replace(/\s/g, ",").split(",");
        s = $.n_loop(s);
        if (s) {
          var j = {};
          if (iA(y, ["+", "-"])) {
            j[y == "-" ? "removeClass" : "addClass"] = s
          } else {
            if (y && !là(y))
              return;
            s.map(function(v) {
              var x = v.replace(/^(-|\+)/, ""),
                á = v.match(/^-/) ? "removeClass" : v.match(/^\+/) ? "toggleClass" : "addClass";
              j[á] = $.merge(j[á] || [], [x])
            })
          }
          $.each(j, function(á, à) {
            t[á](à.join(" "))
          })
        }
      })
    },
    iClass: function(c, t, o, b) {
      return this.each(function() {
        var $t = $(this),
          f = function(x) {
            $t.changeClass(c + (o > 0 ? ",trans" + o : ""));
            if (!x) {
                switch (typeof o) {
                case "function":
                  o();
                  break;
                case "number":
                  setTimeout(function(){
                    $t.changeClass("-trans" + o);
                      (b || rf).call($t)
                  }, o * 100);
                  break
                }
            }
        };
        f(1);
        setTimeout(f, t || 1000)
      })
    },
    _animate: function() { // Hàm chạy animation, có thể áp dụng cho pupop,....
      var ani, u, t, ờ, f = rf, o = {
         prev: is("P") ? is("Android") ? 123 : 89 : 56
      }, 
      m = ["bounceIn", "bounceInDown", "bounceInLeft", "bounceInRight", "bounceInUp", "fadeIn", "fadeInDown", "fadeInDownBig", "fadeInLeft", "fadeInLeftBig", "fadeInRight", "fadeInRightBig", "fadeInUp", "fadeInUpBig", "flipInX", "flipInY", "lightSpeedIn", "rotateIn", "rotateInDownLeft", "rotateInDownRight", "rotateInUpLeft", "rotateInUpRight", "rollIn", "zoomIn", "zoomInDown", "zoomInLeft", "zoomInRight", "zoomInUp", "slideInDown", "slideInLeft", "slideInRight", "slideInUp", "bounce", "flash", "pulse", "tada", "rubberBand", "shake", "headShake", "swing", "wobble", "jello"];
      $.map(arguments, function(v) {
          switch (typeof v) {
          case "string":
            if (v && v != "random")
            ani = v;
          break;
          case "object":
            if ($.isArray(v)) {
              m = v
            } else {
              $.extend(o, v)
            }
          break;
          case "number":
            if (!t) {
                t = v
            } else if (!ờ) {
                ờ = v
            }
          break;
          case "boolean":
            u = v;
            break;
          case "function":
            f = v;
            break;
          }
      });
      return this.each(function() {
        var x = $(this),
          animation_name = ani || Rd(m), 
          pre = o.prev;
        if (x.length) {
          if (u) {
            animation_name = animation_name.replace(/In/, "Out").replace(/Up|Down/, function(name) {
              return {
                U: "Down",
                D: "Up"
              }[name[0]]
            });
            pre += 123
          } else {
            animation_name = animation_name.replace(/Out/, "In")
          }
          x.iClass("+ad1,+" + animation_name, ờ || t).css({
            animationDuration: t > 0 ? t + "ms" : ud,
            display: x.css("display") == "inline" ? "inline-block" : ud
          });
          setTimeout(function() {
            f.call(x);
            setTimeout(function() {
              x.removeCSS("animation-duration")
            }, pre)
          }, (ờ || t || 1000) - pre)
        }
      })
    },
    onShow: function() { // Bắt sự kiện show
      return $(this).show()._animate.apply($(this), arguments)
    },
    onHide: function() { // Bắt sự kiện hide của element, kèm effect, callback
      var hide,
        a = $.map(arguments, function(value) {
          if (typeof value == "function") {
              hide = 1;
              return function() {
                value.apply(this, arguments);
                $(this).hide();
              }
          };
          return value;
        });
      return this.each(function() {
        var f = function() {
          $(this).hide();
        };
        $(this)._animate.apply(
          $(this), $.merge(a, [true, !hide ? f : ud])
        )
      })
    }
  });

  $(window).on('resize', function() { // Responsive
    var width = $(window).width();
    classM(width);
  });
  
  classM($(window).width()); // Nếu có thuộc tính classM thì setup media class
  
  $(".backdrop-effect").self("click", function(){
    $(this).trigger("hideMenu");
  }).on("swiperight", function(){
    $(this).trigger("hideMenu");
  }).on("hideMenu showMenu", function(e){
    var i = $(this),
      a = i.find(".menuMedia"),
      t = e.type == "hideMenu",
      tg = 345; // Thời gian delay trc khi chạy callback
    i.show();
    a[!t ? "onShow" : "onHide"](!t ? "slideOutRight" : "slideInRight", tg, function(){
      i[!t?"fadeIn":"fadeOut"](tg);
    });
  });

  $(document).on("click", ".showMediaMenu", function(){ // Gắn sự kiện click để show menu
    $(".backdrop-effect").trigger("showMenu");
  }).on("click",".hideMediaMenu",function(){
    $(".backdrop-effect").trigger("hideMenu");
  }).on("click", ".menu-item", function(){
    var x = $(this),
      id = x.attr("id"),
      o_t = $("."+id).offset().top;
    if(x.hasClass("isMenuItem"))
      x.addClass("cl1").siblings().removeClass("cl1");
    $("html").stop().animate({scrollTop: o_t - $("#fixed-top").outerHeight()}, 345);
  });

  var ar = $(".list-desktop-menu").find(".menu-item").map(function(v,i) {
    var id = $(i).attr('id'),
      e = $("." + id);
    return {
      id: id, 
      target: v, offset: e.offset().top,
      height: e.outerHeight()
    };
  });
  if(!is("M")){ 
    $(window).scroll(function(){
      var scroll = $(this).scrollTop(),
        f_h = $("#fixed-top").outerHeight() - 3,
        e = true;
      ar.each(function(i,v){
        if(scroll >= (v.offset - f_h) && scroll <= (v.offset + v.height)){
          $('#' + v.id).addClass("cl1").siblings().removeClass("cl1");
          return e = false;
        }
      });
      if(e) $(".menu-item").removeClass("cl1");
    });
  }

});

