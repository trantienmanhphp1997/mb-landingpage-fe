import axios from 'axios'
import { Abstract } from './abstract'
export class Request {
  constructor (abtract = new Abstract()) {
    this.abstract = abtract
    this.commonHeader = this.abstract.getCommonHeader()
    this.commonData = this.abstract.getCommonData()
  }

  post (url, params, header = {}) {
    return new Promise((resolve, reject) => {
      const data = Object.assign(this.commonData, params)
      const config = Object.assign(this.commonHeader, header)
      try {
        axios.post(url, data, config).then((res) => {
          console.log('Request post data results - ', JSON.stringify(res))
          resolve(res)
        }, (err) => {
          reject(err)
        })
      } catch (er) {
        console.log('Post request error - ' + er)
        reject(er)
      }
    })
  }

  get (url, params, header = {}) {
    return new Promise((resolve, reject) => {
      const data = Object.assign(this.commonData, params)
      const config = Object.assign(this.commonHeader, header)
      try {
        axios.get(url, data, config).then((res) => {
          resolve(res)
        }, (err) => {
          reject(err)
        })
      } catch (er) {
        console.log('Get request error - ' + er)
        reject(er)
      }
    })
  }
}
