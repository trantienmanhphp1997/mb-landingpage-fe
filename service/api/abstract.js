export class Abstract {
  constructor () {
    this.token = process.env.VUE_APP_ACCESS_TOKEN || ''
    this.app_key = process.env.VUE_APP_PAGE_ID || ''
  }

  getCommonHeader () {
    return {
      header: {
        'Content-Type': 'application/json;charset=UTF-8',
        Authentication: process.env.access_token
      }
    }
  }

  getCommonData () {
    return {
      app_key: this.app_key
    }
  }
}
