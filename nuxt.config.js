export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'mb-landing',
    htmlAttrs: {
      lang: 'vi'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script:[
      { src: '../assets/js/jquery.min.js', defer: true, type: "text/javascript" }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'ant-design-vue/dist/antd.css',
    'bootstrap/dist/css/bootstrap.min.css',
    '../static/assets/fonts/fontawesome/css/all.min.css',
    '../static/assets/css/style.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/antd-ui',
    '~node_modules/bootstrap'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    [
      '@nuxtjs/dotenv', 
      { filename: '.env.' + process.env.NODE_ENV }
    ],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/dotenv',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'nuxt-fontawesome',
    // [
    //   'nuxt-i18n',
    //   {
    //       locales: [
    //           { code: 'vi', iso: 'vi', file: 'vi.json', name: 'Vietnamese' },
    //           { code: 'en', iso: 'en-US', file: 'en.json', name: 'English' }
    //       ],
    //       defaultLocale: 'vi',
    //       lazy: true,
    //       langDir: 'locales/'
    //   }
    // ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: true
  },

  // Redirect server api
  proxy: {
    '/api/': {
      target: process.env.NODE_ENV == "development" ? 'http://apiato.test/api' : 'http://ladipage.odermanager.tk/api',
      pathRewrite: {
        '^/api/': '/'
      }
    },
    '/storage/': {
      target: process.env.NODE_ENV == "development" ? 'http://apiato.test' : 'http://ladipage.odermanager.tk'
    }
  },

  serer: {
    host: '0.0.0.0'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      config.module.rules.push({
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /(node_modules)/,
        options: {
          fix: true
        }
      })
    }
  }
}
