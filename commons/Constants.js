export default {
  home_menu_data: [
    {
      title: 'Về MBIT',
      class: 'px-3',
      target: 'about-content'
    },
    {
      title: 'Cơ hội nghề nghiệp',
      class: 'px-3',
      target: 'mbit-cup'
    },
    {
      title: 'Học bổng',
      class: 'px-3',
      target: 'studentship'
    },
    {
      title: 'Hợp tác',
      class: 'px-3',
      target: 'required'
    },
    {
      title: 'Đăng ký ngay',
      class: 'btn-register mx-3 px-3 py-3 bg1 lh1 text-white text-center',
      target: 'registerForm'
    }
  ],
  home_slider_data: [
    {
      image: 'assets/img/slide-1.png',
      image_xs: 'assets/img/mobile/slide-1.png',
      title: 'Chương trình <br class="d-lg-none d-xs-block"> học bổng',
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' + "'" + 's standard dummy text ever since the 1500s.',
      isActive: 'active',
      effect: () => {}
    },
    {
      image: 'assets/img/slide-2.png',
      image_xs: 'assets/img/mobile/slide-2.png',
      title: 'Thử sức anh tài cùng Miniapp',
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' + "'" + 's standard dummy text ever since the 1500s.',
      isActive: '',
      effect: () => {}
    },
    {
      image: 'assets/img/slide-3.png',
      image_xs: 'assets/img/mobile/slide-3.png',
      title: 'Ngân hàng TM <br class="d-md-none d-block"> dẫn đầu về <br> Số Hóa',
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' + "'" + 's standard dummy text ever since the 1500s.',
      isActive: '',
      effect: () => {}
    }
  ],
  home_reson_data: [
    {
      title: 'Môi trường cơ sở vật chất hiện đại.',
      img: 'assets/img/icon/w-1.png',
      content: 'Bạn sẽ được hòa mình vào không gian văn phòng hạng A của sự sáng tạo và cơ sở vật chất hiện đại. Cũng trong năm 2021, MB được HR Asia – Tạp chí nhân sự uy tín hàng đầu châu Á trao tặng giải thưởng “Nơi làm việc tốt nhất châu Á”.'
    },
    {
      title: 'Được hòa mình vào tập thể <br> gắn bó, đáng yêu',
      img: 'assets/img/icon/w-2.png',
      content: 'Ở MBIT, chúng tôi biết đâu là điểm mạnh của bạn để phát huy, đâu là điểm yếu để giúp bạn cải thiện. Khi làm việc, bạn có thể hoàn toàn thoải mái trao đổi, tác nghiệp mà không phải theo bất kỳ một quy tắc cứng nhắc nào. '
    },
    {
      title: 'Được hưởng chế độ đãi ngộ <br> xứng đáng',
      img: 'assets/img/icon/w-3.png',
      content: 'MB luôn cố gắng dành những chế độ đãi ngộ tốt nhất dành cho bạn. Từ chế độ lương thưởng đến các gói phúc lợi, bảo hiểm,…tất cả đểu mang những giá trị tốt nhất, tất cả đều thể hiện rằng chúng tôi muốn quan tâm đến bạn nhường nào. '
    },
    {
      title: 'Tiên phong ứng dụng những công nghệ <br> tiên tiến hàng đầu',
      img: 'assets/img/icon/w-4.png',
      content: 'Bạn sẽ liên tục được cập nhật xu hướng CNTT tiên tiến nhất áp dụng và vận hành trong công việc, giúp bạn mở rộng miền tri thức của mình và ứng dụng vào thực tế.'
    },
    {
      title: 'Được đào tạo bài bản với <br> "Lộ trình công danh" rõ ràng',
      img: 'assets/img/icon/w-5.png',
      content: 'Với tầm nhìn là Tập đoàn tài chính đa năng dẫn đầu về chuyển dịch số tại Việt Nam, MB coi đào tạo và phát triển nguồn nhân lực luôn là một trong những ưu tiên hàng đầu, đặc biệt là với nhân sự CNTT. MBIT luôn đồng hành cùng bạn với "Lộ trình công danh" được cá thể hóa với từng người ngay khi vào đây. '
    },
    {
      title: 'Được tiếp xúc, làm việc với đội ngũ lãnh đạo trẻ, <br class="d-md-none d-block"> có năng lực tư duy và hành xử chuyên nghiệp',
      img: 'assets/img/icon/w-6.png',
      content: 'Đội ngũ lãnh đạo của MBIT được đánh giá là một trong những đội ngũ có độ tuổi trung bình trẻ nhất trong hệ thống Ngân hàng, với bề dày kinh nghiệm, tài năng, sức cống hiến và chuyên môn cao trong lĩnh vực CNTT. '
    }
  ],
  home_s_slide: [
    {
      img: 'assets/img/avt.png',
      title: 'Nguyễn Bá Tùng Lâm',
      position: '_Chuyên viên phòng Big data TTPT_',
      content: 'Cuộc thi MBIT CUP đã đem lại cho em nhiều trải nghiệm mới, được cọ xát và tiếp xúc với những sinh viên xuất sắc. Đây cũng là cơ hội giúp em nhận ra được nhiều thiếu sót của mình trong quá trình học tập và làm việc, để từ đó em có thể cải thiện hơn. Cảm ơn MBIT CUP đã cho em được làm việc tại một trong những nơi làm việc tốt nhất Châu Á. Ở phòng và Trung tâm em đang trực thuộc luôn đặc biệt tạo điều kiện cho nhân viên học hỏi những công nghệ mới, tham gia vào những dự án thực tế và được hỗ trợ bởi đội ngũ chuyên gia nhiều năm kinh nghiệm, đây là một trải nghiệm vô cùng tuyệt vời đối với em. ',
      isActive: true
    },
    {
      img: 'assets/img/avt.png',
      title: 'Nguyễn Bá Tùng Lâm',
      position: '_Chuyên viên phòng Big data TTPT_',
      content: 'Cuộc thi MBIT CUP đã đem lại cho em nhiều trải nghiệm mới, được cọ xát và tiếp xúc với những sinh viên xuất sắc. Đây cũng là cơ hội giúp em nhận ra được nhiều thiếu sót của mình trong quá trình học tập và làm việc, để từ đó em có thể cải thiện hơn. Cảm ơn MBIT CUP đã cho em được làm việc tại một trong những nơi làm việc tốt nhất Châu Á. Ở phòng và Trung tâm em đang trực thuộc luôn đặc biệt tạo điều kiện cho nhân viên học hỏi những công nghệ mới, tham gia vào những dự án thực tế và được hỗ trợ bởi đội ngũ chuyên gia nhiều năm kinh nghiệm, đây là một trải nghiệm vô cùng tuyệt vời đối với em. ',
      isActive: true
    }
  ],
  home_review_data: [
    {
      img: 'assets/img/review-1.png',
      title: 'Nhà báo Giang Mèo',
      order: 3
    },
    {
      img: 'assets/img/review-2.png',
      title: 'Hoàng Hoa trung',
      order: 2
    },
    {
      img: 'assets/img/review-3.png',
      title: 'Ca sĩ Thái Thùy Linh',
      order: 1
    },
    {
      img: 'assets/img/review-4.png',
      title: 'Oanh Chan',
      order: 2
    },
    {
      img: 'assets/img/review-5.png',
      title: 'Kiên Trần',
      order: 3
    }
  ],
  home_mbit_content: [
    {
      id: 1,
      title: 'Vòng 1',
      position: 'Phòng cơ sở dữ liệu và ứng dụng',
      department: 'Trung tâm vận hành dịch vụ CNTT',
      image: 'assets/img/r-1.png',
      content: 'Tổ chức hội thảo tại <br> các trường ĐH',
      field_job: 'Mảng Open source database',
      job_description: '<p>+ Hiểu biết về open source database như mysql, nod4j, mongodb, mariadb</p><p>+ Viết tài liệu backup/recovery, an ninh bảo mật cho CSDL</p><p>+ Xây dựng môi trường thử nghiệm cho DB</p>'
    },
    {
      id: 2,
      title: 'Vòng 2',
      position: 'Phòng cơ sở dữ liệu và ứng dụng',
      department: 'Trung tâm vận hành dịch vụ CNTT',
      image: 'assets/img/r-2.png',
      content: 'Tham gia bài thi <br> test tại MB',
      field_job: 'Mảng Open source database',
      job_description: '<p>+ Hiểu biết về open source database như mysql, nod4j, mongodb, mariadb</p><p>+ Viết tài liệu backup/recovery, an ninh bảo mật cho CSDL</p><p>+ Xây dựng môi trường thử nghiệm cho DB</p>'
    },
    {
      id: 3,
      title: 'Vòng 3',
      position: 'Phòng cơ sở dữ liệu và ứng dụng',
      department: 'Trung tâm vận hành dịch vụ CNTT',
      image: 'assets/img/r-3.png',
      content: 'Phỏng vấn trực tiếp <br> các thí sinh',
      field_job: 'Mảng Open source database',
      job_description: '<p>+ Hiểu biết về open source database như mysql, nod4j, mongodb, mariadb</p><p>+ Viết tài liệu backup/recovery, an ninh bảo mật cho CSDL</p><p>+ Xây dựng môi trường thử nghiệm cho DB</p>'
    }
  ],
  props_type: {
    object: {
      default: Object,
      required: true,
      type: Object
    },
    array: {
      default: Array,
      required: true,
      type: Array
    },
    string: {
      default: String,
      required: true,
      type: String
    },
    number: {
      default: Number,
      required: true,
      type: Number
    },
    boolean: {
      default: Boolean,
      required: true,
      type: Boolean
    }
  }
}
